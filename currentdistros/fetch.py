import importlib
import pkgutil

import currentdistros.fetchers


def get_fetcher(distro):
    return FETCHERS.get(distro)


def iter_fetchers():
    return pkgutil.iter_modules(
            currentdistros.fetchers.__path__,
            currentdistros.fetchers.__name__ + ".")


def import_fetchers():
    fetchers = {}
    for finder, modname, ispkg in iter_fetchers():
        _, _, name = modname.rpartition('.')
        module = importlib.import_module(modname)
        # insert fetcher name into module for convenience
        module.name = name
        fetchers[name] = module
    return fetchers


FETCHERS = import_fetchers()
