from datetime import datetime, timedelta
import json


from currentdistros import common
from currentdistros import fetch


SUPPORT_STATES = ['standard', 'lts', 'elts', 'eol']
# try not to spam infrastructure
CACHE_TTL = timedelta(hours=8)


def get_releases(distros=None, cache=True):
    if not distros:
        distros = sorted(fetch.FETCHERS)

    rlss = []
    for distro in distros:
        drlss = None
        if cache:
            drlss = get_cached(distro)
        if not drlss:
            df = fetch.get_fetcher(distro)
            if not df:
                raise ValueError("Invalid distro: %s" % distro)
            drlss = df.fetch_releases()
            if cache:
                cache_store(distro, drlss)

        rlss = rlss + drlss

    return rlss


def cache_store(distro, rlss):
    common.ensure_cache_dir()
    cf_path = common.CACHE_PATH / ('%s.json' % distro)
    cf_path.open('wt').write(dumps(rlss))


def get_cached(distro):
    cf_path = common.CACHE_PATH / ('%s.json' % distro)
    if not cf_path.exists():
        return None
    mtime = datetime.fromtimestamp(cf_path.stat().st_mtime)
    now = datetime.now()
    if now - mtime > CACHE_TTL:
        # ignore old cache
        return None
    data = json.load(cf_path.open('rt'))
    data = json2releases(data)
    return data


def json2releases(data):
    for rls in data:
        if 'released' in rls:
            rls['released'] = common.str2date(rls['released'])
        if 'eol_dates' in rls:
            for k in list(rls['eol_dates'].keys()):
                rls['eol_dates'][k] = common.str2date(rls['eol_dates'][k])
    return data


def rls_str(rls):
    s = "%s %s" % (rls.get('distro'), rls.get('version'))
    cn = rls.get('codename')
    if cn:
        s += ' %s' % cn
    return s


def support_filter(rls, support):
    s = rls.get('support')
    return s in support


def filter_by_support(releases, support):
    return [r for r in releases if support_filter(r, support)]


def print_list(rlss):
    for rls in rlss:
        print(rls_str(rls))


def print_info(rlss):
    for rls in rlss:
        print("%s: %s" % (rls_str(rls), rls['support']))


def dumps(rlss):
    return json.dumps(rlss, indent=2, default=str)
