from datetime import datetime
from pathlib import Path


USER_PATH = Path.home() / '.currentdistros'
CACHE_PATH = USER_PATH / 'cache'


def ensure_cache_dir():
    CACHE_PATH.mkdir(parents=True, exist_ok=True)


def support_state(date, dates):
    std_eol = dates.get('standard')
    if not std_eol or date < std_eol:
        return 'standard'
    lts = dates.get('lts')
    if lts and date < lts:
        return 'lts'
    elts = dates.get('elts')
    if elts and date < elts:
        return 'elts'
    return 'eol'


def str2date(s):
    dt = datetime.strptime(s, r"%Y-%m-%d")
    return datetime.date(dt)
