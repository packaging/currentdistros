import click

from currentdistros import __version__
from currentdistros import fetch
from currentdistros import releases


SHOW_FORMATS = ['info', 'list', 'json']


@click.group()
def cli():
    """
    currentdistros CLI

    What releases are supported for Distro X?
    """


@cli.command()
def available():
    """
    list available distros
    """
    for distro in fetch.FETCHERS:
        print(distro)


@cli.command()
@click.option('-f', '--out-format',
    type=click.Choice(SHOW_FORMATS),
    default=SHOW_FORMATS[0], show_default=True,
    help="select output format")
@click.option('-d', '--distro', 'distros', multiple=True,
    type=click.Choice(sorted(fetch.FETCHERS)),
    help="filter releases by distro(s)")
@click.option('-s', '--support', multiple=True,
    type=click.Choice(releases.SUPPORT_STATES),
    default=['standard', 'lts', 'elts'], show_default=True,
    help="filter releases by support state(s)")
@click.option('--cache/--no-cache',
    default=True, show_default=True,
    help="cache results")
def show(out_format, distros, support, cache):
    """
    show current distro releases
    """
    rlss = releases.get_releases(distros=distros, cache=cache)
    rlss = releases.filter_by_support(rlss, support)
    if out_format == 'json':
        print(releases.dumps(rlss))
    elif out_format == 'info':
        releases.print_info(rlss)
    else:
        releases.print_list(rlss)
    return rlss


if __name__ == '__main__':
    cli()
