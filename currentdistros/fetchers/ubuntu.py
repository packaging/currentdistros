from bs4 import BeautifulSoup
from datetime import datetime
from dateutil.relativedelta import relativedelta
import re
import requests

from currentdistros import common


RELEASE_INFO_URL = 'https://wiki.ubuntu.com/Releases'
RE_DISTRO_RELEASE = r'Ubuntu (\d+\.\d+)(?:\s+LTS)?$'


def fetch_releases():
    """
    fetch Ubuntu releases from wiki
    """
    page = requests.get(RELEASE_INFO_URL)
    soup = BeautifulSoup(page.text, 'html.parser')
    header = soup.find(name='h3', id='Current')
    if not header:
        return []

    now = datetime.date(datetime.now())

    releases = []
    for e in header.next_elements:
        if e.name != 'div':
            continue
        table = e.find(name='table')
        if not table:
            continue

        for row in table.select('tr'):
            cols = row.select('td')
            text = cols[0].text.strip()
            m = re.match(RE_DISTRO_RELEASE, text)
            if not m:
                continue
            version = m.group(1)
            major_version, _, _ = version.partition('.')

            codename = cols[1].text.strip()

            # support end dates and state
            dates = {}
            ss = ubudate(cols[4].text.strip())
            if not ss:
                continue
            dates['standard'] = ss
            lts = ubudate(cols[5].text.strip())
            if lts and lts != ss:
                dates['lts'] = lts
            support = common.support_state(now, dates)
            released = ubudate(cols[3].text.strip())

            rls = {
                'distro': 'Ubuntu',
                'version': version,
                'major_version': major_version,
                'codename': codename,
                'support': support,
                'eol_dates': dates,
                'released': released,
            }
            releases.append(rls)

        # we only check first table after h3#Current header
        break

    return releases


def ubudate(text):
    """
    parse date formats used on Ubuntu wiki
    """
    try:
        dt = datetime.strptime(text, r"%B %d, %Y")
    except ValueError:
        try:
            dt = datetime.strptime(text, r"%B %Y")
        except ValueError:
            return None
    return datetime.date(dt)
