from bs4 import BeautifulSoup
from datetime import datetime
import re
from dateutil.relativedelta import relativedelta
import requests


RELEASE_INFO_URL = 'https://fedoraproject.org/wiki/Releases'
RELEASE_SCHEDULE_URL = 'https://fedorapeople.org/groups/schedule/f-{v}/f-{v}-key-tasks.html'
RE_DISTRO_RELEASE = r'^Fedora (\d+)$'


def fetch_releases():
    """
    fetch current Fedora releases from wiki

    fetch release dates from release schedules

    compute EOL based on Fedora Maintenance Schedule
    """
    page = requests.get(RELEASE_INFO_URL)
    soup = BeautifulSoup(page.text, 'html.parser')
    releases = []
    for e in soup.select('ul > li > b'):
        rls = e.text
        m = re.match(RE_DISTRO_RELEASE, rls)
        if not m:
            continue
        version = m.group(1)
        # parse release date from release schedule
        sch_url = RELEASE_SCHEDULE_URL.format(v=version)
        sch_page = requests.get(sch_url)
        sch_soup = BeautifulSoup(sch_page.text, 'html.parser')
        released = None
        for row in sch_soup.select('tr'):
            cols = row.select('td')
            if len(cols) < 4:
                continue
            if not cols[1].text.startswith("Final Target date"):
                continue
            released = feddate(cols[2].text)
        dates = {}
        if released:
            # calculate EOL based on Fedora Maintenance Schedule  of ~13 months
            dates['standard'] = released + relativedelta(months=13)

        rls = {
            "distro": "Fedora",
            "version": version,
            "major_version": version,
            "support": 'standard',
            "eol_dates": dates,
            "released": released,
        }
        releases.append(rls)

    return releases


def feddate(text):
    """
    parse date format used in Fedora Release Schedule
    """
    try:
        dt = datetime.strptime(text, r"%a %Y-%m-%d")
    except ValueError:
        return None
    return datetime.date(dt)
