import calendar
from datetime import date, datetime
from dateutil.relativedelta import relativedelta

from bs4 import BeautifulSoup
import requests

from currentdistros import common


RELEASE_INFO_URL = 'https://wiki.debian.org/DebianReleases'


def fetch_releases():
    """
    fetch Debian releases from wiki
    """
    page = requests.get(RELEASE_INFO_URL)
    soup = BeautifulSoup(page.text, 'html.parser')
    releases = []
    table = soup.find(name='table')
    if not table:
        return []

    now = datetime.date(datetime.now())
    ancient = date(2000, 1, 1)

    for row in table.select('tr'):
        cols = row.select('td')
        version = cols[0].text.strip()
        major_version, _, _ = version.partition('.')
        codename = None
        a = cols[1].select('a')
        if a:
            codename = a[0].text

        # released date is mandatory
        released = debdate(cols[2].text.strip())
        if not released or released < ancient:
            # ignore too new and too old releases
            continue

        # collect optional EOL dates
        dates = {}
        for col_i, date_tag in (
                (3, 'standard'),
                (4, 'lts'),
                (5, 'elts')):
            try:
                col = cols[col_i]
            except IndexError:
                continue
            col_text = col.text.strip()
            d = debdate(col_text)
            if d:
                dates[date_tag] = d

        support = common.support_state(now, dates)

        rls = {
            'distro': 'Debian',
            'version': version,
            'major_version': major_version,
            'codename': codename,
            'support': support,
            'eol_dates': dates,
            'released': released,
        }
        releases.append(rls)

    return releases


def debdate(text):
    """
    parse date formats used on Debian wiki
    """
    if text.startswith('~'):
        text = text[1:]
    try:
        dt = datetime.strptime(text, "%Y-%m-%d")
        d = datetime.date(dt)
    except ValueError:
        try:
            dt = datetime.strptime(text, "%Y-%m")
            last_day = calendar.monthrange(dt.year, dt.month)[1]
            d = date(dt.year, dt.month, last_day)
        except ValueError:
            try:
                dt = datetime.strptime(text, "%Y")
                d = date(dt.year, 12, 31)
            except ValueError:
                return None

    return d
