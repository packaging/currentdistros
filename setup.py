#!/usr/bin/env python

import setuptools


# get version
exec(open('currentdistros/__init__.py').read())


setuptools.setup(
    version=__version__)
