# currentdistros

`currentdistros` is **going to be** a simple python utility module with sole
purpose of answering the question 

**What releases are currently supported for Distro X?**


## why?

Nearly every automated packaging system needs to know for which platforms it
should build packages at any given time. This information is often hardcoded
somewhere and thus each new distribution release (such as Debian 10 or Fedora
32) leads to updating "current distros" information in huge amount of
redundant files across the software ecosystem.

In order to create a fully automated packaging system, managing/updating
current distro information must be automated as well.


## how?

The same way humans do it. Each distro has a "currently supported releases" webpage in one
format or another:

* [Debian Releases](https://wiki.debian.org/DebianReleases#Current_Releases.2FRepositories)
* [Ubuntu Releases](https://wiki.ubuntu.com/Releases)
* [Fedora Releases](https://fedoraproject.org/wiki/Releases#Current_Supported_Releases)
* etc.

This information can be parsed relatively easily with the help of a web
scrapper such as `BeautifulSoup`.

`currentdistros` should simply become a repository of code for extracting and
managing current distro release information in order to decrease amount of
work associated with new distro releases.


## status

Early alpha. First code has hit the repo.

I'll need this done sooner or later ¯\\\_(ツ)\_/¯ 
